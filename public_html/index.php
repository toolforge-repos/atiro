<?php
require '../src/autoload.php';

header('Content-Type: text/html; charset=utf-8');

function script2lang( string $script ): string {
	switch ( $script ) {
	case 'japanatiras':
		return 'ja';
	case 'koreaiatiras':
		return 'ko';
	case 'kinaiatiras':
		return 'zh';
	case '':
		return '';
	default:
		return 'und';
	}
}

$t = $_GET['t'] ?? null;

$lang = $_GET['lang'] ?? script2lang( $_GET['script'] ?? '' );

switch ( $lang ) {
case '':
	$page = new MainPage();
	break;
case 'ja':
	$page = new JapanAtiro( $t );
	break;
case 'ko':
	$page = new KoreaiAtiro( $t );
	break;
case 'zh':
	$page = new KinaiAtiro( $t );
	break;
default:
	http_response_code( 404 );
	$page = new ErrorPage( $lang );
	break;
}
?>
<!DOCTYPE html>
<html lang="hu">
<head>
	<meta charset="utf-8">
	<title><?= $page->title() ?></title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
<?php $page->printBody(); ?>
</body>
</html>
