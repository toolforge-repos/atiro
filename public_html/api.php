<?php
require '../src/autoload.php';

header( 'Content-Type: text/plain; charset=utf-8' );

if ( empty( $_GET['t'] ) ) {
	http_response_code( 400 );
	echo 'No term specified';
	return;
}

$t = $_GET['t'];

switch ( $_GET['lang'] ?? null ) {
case 'ja':
	$atiro = new JapanAtiro( $t );
	break;
case 'ko':
	$atiro = new KoreaiAtiro( $t );
	break;
case 'zh':
	$atiro = new KinaiAtiro( $t );
	break;
default:
	http_response_code( 400 );
	echo 'Unknown or missing language';
	return;
}

echo $atiro->transliteration();
