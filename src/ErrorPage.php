<?php

class ErrorPage implements Page
{
	private $lang;
	public function __construct( string $lang ) { $this->lang = $lang; }
	public function title(): string { return 'Ismeretlen nyelv'; }
	public function printBody(): void {
?>
<h1>Ismeretlen nyelv</h1>
<p><kbd><?= $this->lang ?></kbd> nyelvről nem tudok átírni, mindentudó azért én se vagyok… <a href=".">Vissza a kezdőlapra.</a></p>
<?php
	}
}
