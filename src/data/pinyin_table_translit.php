<?php
return [
	'initial' => [
		'b' => 'p',
		'c' => 'c',
		'ch'=> 'cs',
		'd' => 't',
		'f' => 'f',
		'g' => 'k',
		'h' => 'h',
		'j' => 'cs',
		'k' => 'k',
		'l' => 'l',
		'm' => 'm',
		'n' => 'n',
		'p' => 'p',
		'q' => 'cs',
		'r' => 'zs',
		's' => 'sz',
		'sh'=> 's',
		't' => 't',
		'w' => 'v',
		'x' => 'hsz',
		'y' => 'j',
		'z' => 'c',
		'zh'=> 'cs',

		'-' => '',
	],

	'final' => [
		'a' => [
			'_' => 'a',
		],
		'ai' => [
			'_' => 'aj',
		],
		'an' => [
			'_' => 'an',
			'y' => 'en',
		],
		'ang' => [
			'_' => 'ang',
		],
		'ao' => [
			'_' => 'ao',
		],
		'e' => [
			'_' => 'ö',

			'-' => 'o',
			'g' => 'o',
			'h' => 'o',
			'k' => 'o',

			'y' => 'e',
		],
		'ei' => [
			'_' => 'ej',
		],
		'en' => [
			'_' => 'en',
		],
		'eng' => [
			'_' => 'eng',
		],
		'i' => [
			'_' => 'i',

			'c' => 'e',
			'ch' => 'e',
			's' => 'e',
			'z' => 'e',
			'zh' => 'e',
		],
		'ia' => [
			'_' => 'ia',
		],
		'ian' => [
			'_' => 'ien',
		],
		'iang' => [
			'_' => 'iang',
		],
		'iao' => [
			'_' => 'iao',
		],
		'ie' => [
			'_' => 'ie',
		],
		'in' => [
			'_' => 'in',
		],
		'ing' => [
			'_' => 'ing',
		],
		'iong' => [
			'_' => 'iung',
		],
		'iu' => [
			'_' => 'iu',
		],
		'o' => [
			'_' => 'o',
		],
		'ong' => [
			'_' => 'ung',
		],
		'ou' => [
			'_' => 'ou',

			'y' => 'u',
		],
		'u' => [
			'_' => 'u',

			'j' => 'ü',
			'q' => 'ü',
			'x' => 'ü',
			'y' => 'ü',
		],
		'ua' => [
			'_' => 'ua',
		],
		'uai' => [
			'_' => 'uaj',
		],
		'uan' => [
			'_' => 'uan',

			'j' => 'üan',
			'q' => 'üan',
			'x' => 'üan',
			'y' => 'üan',
		],
		'uang' => [
			'_' => 'uang',
		],
		'ue' => [
			'_' => 'üe',
		],
		'ui' => [
			'_' => 'uj',
		],
		'un' => [
			'_' => 'un',

			'j' => 'ün',
			'q' => 'ün',
			'x' => 'ün',
			'y' => 'ün',
		],
		'uo' => [
			'_' => 'o',

			'g' => 'uo',
			'h' => 'uo',
			'k' => 'uo',
			'sh' => 'uo',
		],
		'ü' => [
			'_' => 'ü',
		],
		'üan' => [
			'_' => 'üan',
		],
		'üe' => [
			'_' => 'üe',
		],

		// Full syllable as final
		'er' => [
			'_' => 'er',
		],
		'r' => [
			'_' => 'er',
		],
	]
];
