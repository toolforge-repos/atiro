<?php

interface Page
{
	function title(): string;
	function printBody(): void;
}
