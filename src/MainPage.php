<?php

class MainPage implements Page
{
	public function title(): string { return 'Átíró'; }
	public function printBody(): void {
?>
<div style="text-align: center;">
  <h1 style="letter-spacing: -0.1em;">Átíró</h1>
  <p>&rarr; <a href="https://wikipedia.hu/">https://wikipedia.hu/</a></p>
</div>

<p>Avagy:</p>
<ol>
  <li><a href="ja">Hiragana- és katakanaátíró</a></li>
  <li><a href="ko">Hangulátíró</a></li>
  <li><a href="zh">Pinyinátíró</a></li>
  <li><a href="https://aab.qdb.hu/">Átírási adatbázis</a></li>
</ol>
<?php
	}
}
