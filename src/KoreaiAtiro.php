<?php

/**
 * Lingua::KO::Romanize::Hangul alapjan,
 * lasd http://www.kawa.net/works/perl/romanize/romanize-e.html
 *
 * 2007. december 7.
 *
 * Osvath Gabor: Koreai nyelv alapfokon I. Budapest, 1995.
 *   \o.  ez alapjan ambivalenciak kikuszobolese
 *
 * 2008. marcius 25.
 */
class KoreaiAtiro extends Atiro {
	private const INITIAL_LETTER = [
		'[k/g]', 'kk', 'n', '[t/d]', 'tt', '[r/l]', 'm', '[p/b]', 'pp',
		'sz', 'ssz', '', '[cs/dzs]', 'ccs', 'csh', 'kh', 'th', 'ph', 'h'
	];
	private const PEAK_LETTER = [
		'a', 'e', 'ja', 'je', 'o', 'e', 'jo', 'je', 'o', 'va', 've',
		'ö', 'jo', 'u', 'vo', 've', 'ü', 'ju', 'u', 'i', 'i'
	];
	private const FINAL_LETTER = [
		'', '[k/g]', 'kk', '[k/g]sz', 'n', 'n[cs/dzs]', 'nh', '[t/d]',
		'[r/l]', '[r/l][k/g]', '[r/l]m', '[r/l][p/b]', '[r/l]sz',
		'[r/l]th', '[r/l]ph', '[r/l]h', 'm', '[p/b]', '[p/b]sz', 'sz',
		'ssz', 'ng', '[cs/dzs]', 'csh', 'kh', 'th', 'ph', 'h'
	];

	public function lang(): string { return 'ko'; }
	public function title(): string { return 'Koreaiátíró webes alkalmatosság'; }
	public function slogan(): string { return 'Hangulból magyarosra, az OH-i ajánlás alapján'; }
	public function transliteration(): string {
		# [UCS-2] AC00-D7A3
		# [UTF-8] EAB080-ED9EA3
		# EA-ED are appeared only as Hangul's first character.
		preg_match_all(
			'/([\xEA-\xED][\x80-\xBF]{2})|([^\xEA-\xED]+)/s',
			$this->original,
			$matches,
			PREG_SET_ORDER | PREG_UNMATCHED_AS_NULL
		);
		$array = array_map(
			function (array $match): string {
				list(, $hangul, $nonhangul) = $match;
				if ($hangul !== null) {
					return $this->char($hangul) ?? $hangul;
				} else {
					return $nonhangul;
				}
			},
			$matches
		);
		return htmlspecialchars($this->disambiguate(implode($array)));
	}
	public function printNotes(): void {
?>
<!--<p>Szóhatárokon (pl. vezetéknév és keresztnév közé) illessz be egy szóközt, ha nincs, mert torzulhat a végeredmény.</p>-->

<p>Kis koreai hangtan Osváth Gábor alapján:</p>
<ol>
  <li>Zöngétlen hangok magánhangzók között és nazális hangok után zöngéssé válnak.</li>
  <li>Az &#12601; betűvel jelölt fonéma magánhangzók között [r], mássalhangzók előtt és szó végén [l]. (Szó elején csak idegen szavakban fordul elő.)</li>
</ol>

<p>Ezek alapján nagyjából el tudja dönteni, hogy hogyan kell a hangult átírni. Ha valahol mégis bizonytalan és több ötlete is van, ott nem tudja, hogy melyik volna kiejtés szerint helytálló. Ha másból nem, McCune&ndash;Reischauerből átbetűzheted a maradékot (lásd <a href="https://hu.wikipedia.org/wiki/WP:KOREAI">a Wikipédia útmutatóját</a>).</p>
<?php
	}

	/**
	 * Disambiguate letters that are ambiguous on their own, using the context.
	 * @param string $string The text containing the ambiguous letters with `[a/b]` syntax
	 * @return string The unambiguous text, containing no braces anymore
	 */
	private function disambiguate(string $string): string {
		# Zongetlen hangok mgh.-k kozott & nazalis hangok utan
		# zongesse valnak.
		#
		# Look-ahead assertion nelkul "Cso Dzsedzsin" Dzsecsin marad. :-(
		$string = preg_replace('#(ö|ü|[aeiou])\[(k|cs|t|p)/(g|dzs|d|b)\](?=ö|ü|[aeiou])#', '$1$3', $string);
		$string = preg_replace('#(m|n|ng)\[(k|cs|t|p)/(g|dzs|d|b)\]#', '$1$3', $string);
		$string = preg_replace('#\[(k|cs|t|p)/(g|dzs|d|b)\]#', '$1', $string);

		# Az r/l fonema mgh.-k kozott [r], msh.-k elott & szo vegen [l].
		# Szo elejen csak idegen szavakban fordul elo.
		$string = preg_replace('#\[r/l\](?=[\[bcdfghjklmnpqrstvwxyz]|\W|$)#', 'l', $string);
		$string = str_replace('[r/l]', 'r', $string);
		return $string;
	}

	/**
	 * Transliterate a single Hangul character.
	 * @param string $char A single Hangul character in UTF-8 (3 bytes)
	 * @return string The transliteration of the Hangul character, or `null` in case of an error.
	 */
	private function char(string $char): ?string {
		@list($c1, $c2, $c3, $c4) = array_map('ord', str_split($char));
		if ($c3 === null || $c4 !== null) return null;
		$ucs2 = (($c1 & 0x0F)<<12) | (($c2 & 0x3F)<<6) | ($c3 & 0x3F);
		if ($ucs2 < 0xAC00 || $ucs2 > 0xD7A3) return null;
		$han  = $ucs2 - 0xAC00;
		$init = (int)( $han / 21 / 28 );
		$peak = (int)( $han / 28 ) % 21;
		$fin  = $han % 28;
		return self::INITIAL_LETTER[$init] . self::PEAK_LETTER[$peak] . self::FINAL_LETTER[$fin];
	}
}
