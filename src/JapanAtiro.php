<?php

class JapanAtiro extends Atiro
{
	public function lang(): string { return 'ja'; }
	public function title(): string { return 'Hiragana- és katakanaátíró'; }
	public function slogan(): string { return 'A néha fura rozsdás mankó (nem csak) firefoxosoknak'; }

	/**
	 * Encode all characters. This prevents XSS attacks on the one hand,
	 * and also makes it possible to keep data file use HTML entities.
	 */
	private function encoded(): string {
		$to = mb_convert_encoding($this->original, 'UTF-32', 'UTF-8');
		$to = unpack('N*', $to);
		$to = array_map(function ($n) { return "&#$n;"; }, $to);
		return implode('', $to);
	}

	public function transliteration(): string {
		list( $kana, $kana_hosszu ) = require 'data/japan.php';

		$t = $this->encoded();
		foreach (array(16, 8) as $l) {
			foreach ($kana as $k => $a) {
				if (strlen($k) == $l) {
					$t = str_replace($k, $a, $t);
				}
			}
		}

		foreach ($kana_hosszu as $k => $a) {
			$t = preg_replace("/$k/", $a, $t);
		}
		return $t;
	}

	public function printNotes(): void {
?>
<p>Az OH (247. o.) javaslatát követve a Hepburnben &bdquo;z&rdquo;-vel jelölt hangokat nem írja át &bdquo;dz&rdquo;-re.</p>

<p>Van néhány dolog, amit nem tud:</p>
<ul>
	<li>Néhány olyan katakana-összetételt nem ismer, amit idegen szavak leírására használnak.</li>
	<li>Nem ismeri fel azokat a speciális eseteket, amikor mondatban a &bdquo;ha&rdquo; partikulát (&#12399;) &bdquo;va&rdquo;-nak ejtik, pl. &bdquo;Vatasi no namae <i>va</i> hanamidzsin deszu.&rdquo;</li>
	<li>Előfordul, hogy az &bdquo;o&rdquo; hangzó után hosszabbításnak vélt &bdquo;u&rdquo; kana (&#12358;) egy különálló kandzsi első hangja, ezért nem vonandó össze az ó-val, de ezt ő nem tudja, pl. &bdquo;Szakanoue&rdquo; (&#22338;&#19978;).</li>
</ul>
<?php
	}

}
