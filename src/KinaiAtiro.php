<?php

class KinaiAtiro extends Atiro
{
	private const RE_SYL = '/^([^aeiouü]*)(.*)/';
	private const RE_WORD = '/^([^a-zü]*)([a-zü\'-]*)(.*)/';

	/** @var array<int,string> */
	private $pinyin_table;
	/** @var array{initial:array<string,string>,final:array<string,array<string,string>>} */
	private $translit_table;

	public function lang(): string { return 'zh'; }
	public function title(): string { return 'Kínai szavak átírója'; }
	public function slogan(): string { return 'Pinyinből, szépen'; }
	public function transliteration(): string {
		$this->pinyin_table = $this->pinyin_table ?? require 'data/pinyin_table.php';
		$this->translit_table = $this->translit_table ?? require 'data/pinyin_table_translit.php';

		$chunks = preg_split('/\s+/', mb_strtolower($this->original));
		$chunks_t = [];
		foreach($chunks as $chunk) {
			$matches = [];
			if (preg_match(self::RE_WORD, $chunk, $matches)) {
				$word_t = $this->translit_word($matches[2]);
				$chunks_t[] = $matches[1] . $word_t . $matches[3];
			}
		}
		return htmlspecialchars(implode(' ', $chunks_t));
	}
	public function printNotes(): void {
?>
<p>Nem árt tudni hozzá:</p>
<ul>
<li>Személynevekben a kéttagú keresztnevek szótagjai közé magyaros átírásban kötőjel kerül. Pinyinben nem, ezt utólag kell belegyömöszkölni, például: <i>Yang Luchan &rarr; jang lucsan &rarr; Jang Lu-csan.</i></li>
<li>Ha kevésbé egyértelmű, hogy egy szótagnak hol van a határa, belekavarodhat és magyaros helyett a pinyint dobhatja vissza. Ilyenkor aposztróffal vagy kötőjellel érdemes jelezni, mire is gondoltunk: <i>Wanguo &rarr; Wan'guo.</i>
</ul>

<p>Az OH-i alapokon nyugvó útmutatás, amit ez a script is követ: <a href="https://hu.wikipedia.org/wiki/Wikip%C3%A9dia:K%C3%ADnai_nevek_%C3%A1t%C3%ADr%C3%A1sa">kínai nevek átírási útmutatója a Wikipédián.</a></p>
<?php
	}

	/**
	 * Helper function of transliteration().
	 *
	 * Transliterate a word of pinyin.
	 */
	private function translit_word(string $str): string {
		// Pseudo-syllables actually, as they
		// have to be taken apart later.
		$syllables = preg_split("/['-]/", $str);

		$str_t = '';
		foreach($syllables as $syllable) {
			while ($syllable) {
				$found_pinyin = false;
				foreach ($this->pinyin_table as $p) {
					if (strpos($syllable, $p) === 0) {
						$str_t .= $this->translit_syl($p);
						$syllable = substr($syllable, strlen($p));
						$found_pinyin = true;
						break;
					}
				}
				// If no valid pinyin found, leave whole word untransliterated
				if (!$found_pinyin) {
					return $str;
				}
			}
		}
		return $str_t;
	}

	/**
	 * Helper function for translit_word().
	 *
	 * Transliterate a syllable of pinyin.
	 */
	private function translit_syl(string $str): string {
		$translit_initial_table = $this->translit_table['initial'];
		$translit_final_table = $this->translit_table['final'];
		if ($str === 'er' || $str === 'r') {
			// Er/r can behave as a separate syllable
			$initial = '-';
			$final = 'r';
		} else {
			// Otherwise apply pattern to obtain initial and final
			$syl = [];
			if (!preg_match(self::RE_SYL, $str, $syl)) {
				return $str;
			}
			$initial = $syl[1] ?: '-';
			$final = $syl[2];
		}

		// Can’t transliterate if initial or final is not in the table
		if (!isset($translit_initial_table[$initial]) || !isset($translit_final_table[$final])) {
			return $str;
		}

		// Transliteration
		$initial_t = $translit_initial_table[$initial];

		// Check if initial alters transliteration of final
		$final_t = $translit_final_table[$final][$initial] ?? $translit_final_table[$final]['_'];

		return $initial_t . $final_t;
	}
}
