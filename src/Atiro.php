<?php

abstract class Atiro implements Page
{
	/** @var string|null The original text, as sent by the user */
	protected $original;

	public function __construct(?string $original) {
		$this->original = $original;
	}

	/** BCP-47 language code of the transliterated language */
	public abstract function lang(): string;
	public abstract function slogan(): string;
	/** Transliterate the string. This is called only when $original is not `null`. */
	public abstract function transliteration(): string;
	public function printNotes(): void {}
	public function printBody(): void {
?>
<a href=".">Kezdőlap</a>
<h1><?= $this->title() ?><br><small><?= $this->slogan() ?></small></h1>
<?php if ( !empty( $this->original ) ) : ?>
<p><span lang="<?= $this->lang() ?>"><?= htmlspecialchars( $this->original ) ?></span> magyarul <i><?= $this->transliteration() ?></i></p>
<?php endif; ?>
<form method="GET">
	<input type="text" name="t" value="<?= htmlspecialchars( $this->original ) ?>" required>
	<input type="submit" value="Mehet">
</form>

<?php
		$this->printNotes();
	}
}
